# TP2 Docker compose et Docker swarm

## 1. Docker compose

### Dockerhub

![](https://i.imgur.com/uAq6585.png)

### Un premier docker compose

```
version: "3"
services:

  db:
    image: lemarquiis/tp1_db
    container_name: mariadb-container
    ports:
      - "3306:3306"
    networks:
      - mynetwork
    volumes:
      - /home/marc/Labo-Cloud-2020-2021/ressources/docker/tp:/var/lib/mysql/

  node:
    image: lemarquiis/tp1
    ports:
      - "80:3000"
    networks:
      - mynetwork
    depends_on: 
        - db

networks: 
    mynetwork:
```

* Le version signifie la version qu'on veut utiliser dans ce cas le 3 signifie qu'il va prendre la dernière version lts, les services sont les containers qu'on souhaites créer dans ce cas une bdd et un node, les commandes inclus dans les containers restent les mêmes que sur le dockerfile créer sur le tp précédent, et networks nous permet de créer notre réseau.

### Un cas pratique

```
version: "3"

services :

  db:
    image: mariadb:latest
    container_name: wp_mariadb
    restart: always
    volumes:
      - ./db:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    ports:
      - "3306:3306"
    networks:
      - wordpress


  wordpress:
    depends_on:
      - phpmyadmin
    image: wordpress:latest
    container_name: wp_wordpress
    restart: always
    volumes: ["./wordpress:/var/www/html"]
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
    ports:
      - "80:80"
    networks:
      - wordpress

  phpmyadmin:
    depends_on:
      - db
    image: phpmyadmin:latest
    container_name: wp_phpmyadmin
    restart: always
    environment:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: password
    volumes:
      - ./phpmyadmin:/etc/phpmyadmin
    ports:
      - "8080:80"
    networks:
      - wordpress

networks:
  wordpress:

```

* Ce docker-compose est constitué des 3 conteneurs qu'il fallait pour cet exercice dont le wordpress la bdd et le phpmyadmin, tous sont dans le réseau créé (wordpress), les conteneurs se demarre dans un ordre precis bdd->phpmyadmin->wordpress, cela fonctionne tres bien comme peut vous montrez les screens si dessous.
* Le Wordpress

![](https://i.imgur.com/xA0houO.png)

* Le phpmyadmin

![](https://i.imgur.com/jBzFnRF.png)

* Voici les dossiers de l'hôte 

![](https://i.imgur.com/WE2agoi.png)

![](https://i.imgur.com/GBUQfua.png)

![](https://i.imgur.com/bZ3Oo5t.png)

## Docker swarm
### initialisation du swarm

```
[root@localhost ~]# docker node ls
ID                            HOSTNAME                STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
7z38h8nwgfdke73hxflzt7vcj *   localhost.localdomain   Ready               Active              Leader              19.03.13
gc1b13keikl42ebr3k6qidljq     localhost.localdomain   Ready               Active                                  19.03.13
gdiegn80lt94xklqhofsg2vlb     localhost.localdomain   Ready               Active                                  19.03.13
```

### Une première application orchestrée

* Voici l'appli

```
[marc@m1 stack]$ docker stack deploy -c docker-compose.yml test
Creating network test_default
Creating service test_web
Creating service test_redis
[marc@m1 stack]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                   PORTS
q66q15rdfl40        test_redis          replicated          0/1                 redis:alpine
qx5bq97o24r5        test_web            replicated          1/1                 lemarquiis/test_stack:latest   *:8080->5000/tcp

```

* curl depuis la m2 

```
[marc@m2 ~]$ curl 10.2.1.2:8080
tout en 1 ficher on m'a vu 2 fois.
```

* curl depuis la m3

```
[marc@m3 ~]$ curl 10.2.1.2:8080
tout en 1 ficher on m'a vu 3 fois.
```

* Liste des stacks

```
[marc@m1 stack]$ docker stack ls
NAME                SERVICES            ORCHESTRATOR
test                2                   Swarm
```

* Liste des services 

```
[marc@m1 stack]$ docker stack services test
ID                  NAME                MODE                REPLICAS            IMAGE                   PORTS
q66q15rdfl40        test_redis          replicated          1/1                 redis:alpine
qx5bq97o24r5        test_web            replicated          1/1                 lemarquiis/stack:latest   *:8080->5000/tcp

```

* Si on augmente le nombre de service web

```
[marc@m1 stack]$ docker service scale test_web=3
test_web scaled to 3
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged
[clement@m1 stack]$ docker stack services test
ID                  NAME                 MODE                REPLICAS            IMAGE                   PORTS
kidt5zznafn0        test_web             replicated          3/3                 lemarquiis/stack:latest
m4zijmibai1x        test_redis           replicated          1/1                 redis:alpine
```

### Centraliser l'accès au service

* Avec l'exemple du docker-compose fournis voici le résultat du service.

![](https://i.imgur.com/5HizeRd.png)

* Pour lié les 2 services voici le docker-compose utilisé

```
[marc@m1 stack]$ cat docker-compose.yml
version: '3'
services:
  reverse-proxy:
    image: traefik:v2.1.3
    command:
      - "--providers.docker.endpoint=unix:///var/run/docker.sock"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.network=traefik"
      - "--entrypoints.web.address=:80"
      - "--api.dashboard=true"
      - "--api.insecure=true"
    ports:
      - 8080:8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - traefik
    deploy:
      placement:
        constraints:
          - node.role == manager
  web:
    image: lemarquiis/test_stack:latest
  redis:
    image: "redis:alpine"

    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.webapp.rule=Host(`webapp`)"
        - "traefik.http.routers.webapp.entrypoints=web"
        - "traefik.http.services.webapp.loadbalancer.server.port=3000"
networks:
  traefik:
    external: true

```

![](https://i.imgur.com/iDWUZY2.png)

* Thats work !
* ps : un 2 ème 22/20 ? ;)