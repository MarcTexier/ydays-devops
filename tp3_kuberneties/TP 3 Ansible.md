# TP 3 : Ansible
## Configuration de l'environement
## inventory
```
all:
  hosts:
    admin.ydays:  
  children:
    worker:
      hosts:
        nodeworker1.ydays: 10.0.0.3
        nodeworker2.ydays: 10.0.0.4
    master:
      hosts:
        nodemaster.ydays: 10.0.0.2
```
* ce fichier liste tous les workers et la node master u tp avec leurs ip.
### Common 

* Nous pouvons voir le ping fonctionner 
```
10.0.0.3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
10.0.0.4 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
``` 

### role common
```
- name: Install some packages
  hosts: master, worker
  tasks:
    - name : install packages
      apt:
        name: apt-transport-https, ca-certificates, curl, gnupg-agent, software-properties-common
        state: present
        update_cache: yes
  become: yes
```

* On indique dans le fichier dans quel machine on souhaite installer les paquets et lesquels et on mets a jour le cache avant de l'installer

## Role Docker
### role docker
```
- name: Install docker
  hosts: master, worker
  tasks:
    - name: Add Docker apt repository key.
      apt_key:
        url: "https://download.docker.com/linux/ubuntu/gpg"
        state: present
    - name: Set the stable docker repository
      apt_repository: 
        repo: "deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_lsb.codename }} edge"
        state: present
        update_cache: yes
    - name : install docker-ce
      apt:
        name: docker-ce,docker-ce-cli,containerd.io
        state: present
    - name: 
      user: 
        name: ansible
        group: docker
  become: yes
```
* On indique dans le fichier dans quel machine on souhaite installer docker on a suivi la prcédure d'un ancien tp et adapté à celui ci
## role master
### role master
```
- name: Kubernetes Master
  hosts: master
  tasks:
    - name: allow 'ansible' to have passwordless sudo
      lineinfile:
        dest: /etc/sudoers
        line: 'ansible ALL=(ALL) NOPASSWD: ALL'
        validate: 'visudo -cf %s'
    - name: disable SWAP
      mount:
        name: "{{ item }}"
        fstype: swap
        state: absent
      with_items:
        - swap
    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb > 0
    - name: Add kubernetes apt repository key.
      apt_key:
        url: "https://packages.cloud.google.com/apt/doc/apt-key.gpg"
        state: present
    - name: Set the stable kubernetes repository
      apt_repository:
        repo: "deb https://apt.kubernetes.io/ kubernetes-xenial main"
        state: present
        update_cache: yes
    - name : install kubernetes
      apt:
        name: kubelet, kubeadm, kubectl
        state: present

    - name: Configure node ip
      lineinfile:
        path: '/etc/systemd/system/kubelet.service.d/10-kubeadm.conf'
        line: 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.2"'
        regexp: 'KUBELET_EXTRA_ARGS='
        insertafter: '\[Service\]'
        state: present
    - name: Restart kubelet
      service:
        name: kubelet
        daemon_reload: yes
        state: restarted
    - name: Initialize the Kubernetes cluster using kubeadm
      command: kubeadm init --apiserver-advertise-address="10.0.0.2" --apiserver-cert-extra-sans="10.0.0.2"  --node-name="nodemaster" --pod-network-cidr=10.0.0.0/16

    - name: Setup kubeconfig for ansible user
      command: "{{ item }}"
      with_items:
       - mkdir -p /home/ansible/.kube
       - cp -i /etc/kubernetes/admin.conf /home/ansible/.kube/config
       - chown ansible:docker /home/ansible/.kube/config
    - name: Install calico pod network
      become: false
      command: kubectl create -f https://docs.projectcalico.org/v3.4/getting-started/kubernetes/installation/hosted/calico.yaml

    - name: Generate join command
      command: kubeadm token create --print-join-command
      register: join_command

    - name: Copy join command to local file
      local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"
  handlers:
    - name: docker status
      service: name=docker state=started
  become: yes
```
* On indique dans le fichier dans quel machine on souhaite installer les paquets et on fait l'installation de kubernetes sur la master mais tout ce n'est pas passé comme prévue a cause des bugs au niveau de l'install de calico
## role worker
### role worker
```
- name: Kubernetes Master
  hosts: worker
  tasks:
    - name: allow 'ansible' to have passwordless sudo
      lineinfile:
        dest: /etc/sudoers
        line: 'ansible ALL=(ALL) NOPASSWD: ALL'
        validate: 'visudo -cf %s'
    - name: disable SWAP
      mount:
        name: "{{ item }}"
        fstype: swap
        state: absent
      with_items:
        - swap
    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb > 0
    - name: Add kubernetes apt repository key.
      apt_key:
        url: "https://packages.cloud.google.com/apt/doc/apt-key.gpg"
        state: present
    - name: Set the stable kubernetes repository
      apt_repository:
        repo: "deb https://apt.kubernetes.io/ kubernetes-xenial main"
        state: present
        update_cache: yes
    - name : install kubernetes
      apt:
        name: kubelet, kubeadm, kubectl
        state: present

    - name: Configure node ip
      lineinfile:
        path: '/etc/systemd/system/kubelet.service.d/10-kubeadm.conf'
        line: 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.2"'
        regexp: 'KUBELET_EXTRA_ARGS='
        insertafter: '\[Service\]'
        state: present
    - name: Restart kubelet
      service:
        name: kubelet
        daemon_reload: yes
        state: restarted
    - name: Initialize the Kubernetes cluster using kubeadm
      command: kubeadm init --apiserver-advertise-address="10.0.0.2" --apiserver-cert-extra-sans="10.0.0.2"  --node-name="nodemaster" --pod-network-cidr=10.0.0.0/16

    - name: Setup kubeconfig for ansible user
      command: "{{ item }}"
      with_items:
       - mkdir -p /home/ansible/.kube
       - cp -i /etc/kubernetes/admin.conf /home/ansible/.kube/config
       - chown ansible:docker /home/ansible/.kube/config
    - name: Copy the join command to server location
      copy: src=join-command dest=/tmp/join-command.sh mode=0777

    - name: Join the node to cluster
      command: sh /tmp/join-command.sh


  become: yes
```

* Etant donné que sur la master cela ne fonctionne pas nous pouvons pas garantir quer le fichier worker donctionne egalement.

* Ce tp était sympa mais tres hard soyez plus cool la prochaine fois ;)
