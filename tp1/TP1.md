# TP1 Une introduction a Docker

![](https://i.imgur.com/I1JyA5x.png)

## Prendre en main les commandes Docker

🚩 affichez la liste des images docker sur votre hôte.

```
PS C:\Users\33623> docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              bf756fb1ae65        9 months ago        13.3kB
tutum/hello-world   latest              31e17b0746e4        4 years ago         1
```
🚩 affichez la liste des conteneurs.
```
PS C:\Users\33623> docker container ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                   NAMES
4a2f2b868508        tutum/hello-world   "/bin/sh -c 'php-fpm…"   6 minutes ago       Up 6 minutes        0.0.0.0:32778->80/tcp   hello-world
```
🚩 affichez tout les fichiers dans /www.

```
/www # ls
index.php  logo.png
```

🚩 arrêtez le conteneur.
```
PS C:\Users\33623> docker kill 4a2f2b868508
4a2f2b868508
```

🚩 affichez la liste des conteneurs docker avec leurs status.
```
docker container ls
```
🚩 supprimez le conteneur nommé hello-world.
```
docker container kill "id"
```

## Prise en Main du Dockerfile

```
PS C:\Users\33623\Desktop\Labo-Cloud-2020-2021\ressources\docker\tp> docker build --tag npm .
[+] Building 0.1s (9/9) FINISHED
 => [internal] load build definition from Dockerfile                                                                              0.1s
 => => transferring dockerfile: 519B                                                                                              0.0s
 => [internal] load .dockerignore                                                                                                 0.0s
 => => transferring context: 2B                                                                                                   0.0s
 => [internal] load metadata for docker.io/library/node:14.7-alpine3.12                                                           0.0s
 => [1/4] FROM docker.io/library/node:14.7-alpine3.12                                                                             0.0s
 => [internal] load build context                                                                                                 0.0s
 => => transferring context: 701B                                                                                                 0.0s
 => CACHED [2/4] WORKDIR /app                                                                                                     0.0s
 => CACHED [3/4] COPY ./app/ /app                                                                                                 0.0s
 => CACHED [4/4] RUN npm install                                                                                                  0.0s
 => exporting to image                                                                                                            0.0s
 => => exporting layers                                                                                                           0.0s
 => => writing image sha256:2b72b59756f3d0153873a3b15961d6ca66fc97eee1827646569e3da2d6f8dfaf                                      0.0s
 => => naming to docker.io/library/npm
```

🚩 Faites une capture d'écran de la la page web qui tourne sur 127.0.0.1 avec l'url inclus.
![](https://i.imgur.com/fwEroNm.png)

## communication et gestion de volumes
![](https://i.imgur.com/ylQLPI7.png)

Pour faire communiquer les 2 conteneurs j'ai créer un réseau privé bridge appeler mon-bridge
```
ocker network inspect mon-bridge
[
    {
        "Name": "mon-bridge",
        "Id": "3edb762549d0990c685c4ca5b888b19cfc2e2335c0f1cbcbf6d24af321c0fad1",
        "Created": "2020-10-14T22:09:08.811620888+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "4ecb4a11f073942cc4ecc06e1013dfe9aac2e54ff66eb1653ccfe66c9cbe483e": {
                "Name": "tp1_db",
                "EndpointID": "c97e814576040164f24faaaaf7176947c60f70450618cc121ea7c08e32343ad1",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "c3bb4906721a4813a9a966da5bb2305ba40e2549968eb0357c027e790ba1d3d0": {
                "Name": "tp1",
                "EndpointID": "daaad5f95f6e41a03a219a7736f0b4dde457ce3273db79a5380c9f567f3c3d6a",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]

```
Le Docker file est dans le répo ;)

Pour build le conteneur 
```
docker image build -t tp1_db .
```

Pour le lancer : 
```
docker run --name mariadb-container -d -v /home/marc/Documents/Labo-Cloud-2020-2021/ressources/docker/tp/db:/var/lib/mysql:/var/lib/mysql/ -p 3306:3306 --network mon-bridge tp1_db
```

Resultat:

![](https://i.imgur.com/owZlmY5.png)


Merci pour le 22/20 :)